 
class Add{
    int add(int a, int b){
        return a+b;
    }
    double add(double a, double b){
        return a+b;
    }
    int add(int a,int b, int c){
        return a+b+c;
    }
}
public class MethodOLJava {
    public static void main(String[] args) {
       Add m = new Add();
        System.out.println("The sum = "+m.add(2,3));
        System.out.println("The sum = "+m.add(2.2,3.3));
        System.out.println("The sum = "+m.add(2,3,5));
    }    
}
