abstract class Base {
    abstract void fun();
    void display(){
        System.out.println("Base class");
    }

}
class Derived extends Base {
	void fun()
	{
		System.out.println("Derived fun() called");
	}
}
class  ParAbsJava{
public static void main(String args[])
	{
		Derived b = new Derived();
        b.display();
        b.fun();

	}
}
