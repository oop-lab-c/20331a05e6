class Base{
    void addFunction(int a,int b){
        System.out.println("This is base class function");
    }
}
class Derived extends Base{
    void addFunction(int a,int b){
        System.out.println("This is derived class function");
   }
    
}
public class MethodORJava {
    public static void main(String[] args) {
        Derived obj=new Derived();
        obj.addFunction(5, 10);

    }
}