#include<iostream>
using namespace std;
class opoverload
{
	private:
		int r,img;
	public:
		opoverload(int real=0 ,int im=0)
		{
			r = real;
			img = im;
		}
		opoverload operator + (opoverload const &obj)
		{
			opoverload result;
			result.r = r + obj.r;
			result.img = img + obj.img;
			return result;
		}
		void print()
		{
			cout<<r<<"+ i"<<img<<endl;
		}
};
int main()
{
	opoverload obj(5,6);
	opoverload obj1(2,3);
	opoverload obj2 = obj + obj1;
	obj2.print();
}