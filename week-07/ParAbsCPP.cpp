#include<iostream>
using namespace std;
class Animal
{
	public:
		virtual void bark()
		{
			cout<<"bark"<<endl;
		}
		virtual void walk()
		{
			
		}
};
class dog :public Animal
{
	public:
		void walk()
		{
			cout<<"walk "<<endl;
		}
};
int main()
{
	dog obj;
	obj.walk();
	obj.bark();
return 0;
}