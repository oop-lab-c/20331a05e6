#include<iostream>
using namespace std;
class base
{
	public:
		int pub = 4;
	private:
	    int priv = 5; 
	protected:
		int prot = 6;
	public:
		int getpriv()
		{
			return priv  ;
		}

};
class pubderived: public base
{
	public:
		int getpub()
		{
			return pub;
		}
		int getprot()
		{
			return prot;
		}		
};
class protderived: protected base
{
	public:
		int getpub()
		{
			return pub;
		}
		int getprot()
		{
			return prot;
		}

};
class privderived: private base
{
	public:
		int getpub()
		{
			return pub;
		}
		int getprot()
		{
			return prot;
		}

};
int main()
{
	pubderived obj;
cout<<"public = "<<obj.getpub()<<endl;
cout<<"protected = "<<obj.getprot()<<endl;
cout<<"private value cannot be accessed"<<endl;
	protderived obj1;
cout<<"public = "<<obj1.getpub()<<endl;
cout<<"protected = "<<obj1.getprot()<<endl;
cout<<"private value cannot be accessed"<<endl;
	privderived obj2;
cout<<"public = "<<obj2.getpub()<<endl;
cout<<"protected = "<<obj2.getprot()<<endl;
cout<<"private value cannot be accessed"<<endl;
}