
interface grandparent
{
    public default void display()
    {
        System.out.println("I am grandparent");
    }
}
interface parent1 extends grandparent
{

}
interface parent2 extends grandparent{

}
public class MultipleInheriJava implements parent1,parent2 {
    public static void main(String[] args)
    {
        MultipleInheriJava obj = new MultipleInheriJava();
        obj.display();
    }
}
