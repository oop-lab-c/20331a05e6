#include<iostream>
using namespace std;
class parent1
{
	public:
		parent1()
		{
			cout<<"i am parent1 "<<endl;
		}
};
class parent2
{
	public:
		parent2()
		{
			cout<<"i am parent2 "<<endl;
		}
};
class singleinher : public parent1
{
	public:
		singleinher()
		{
	cout<<"i am child of parent1"<<endl;
}
};
class multipleinher : public parent1 ,public parent2
{ 
public:
	multipleinher()
	{
cout<<"i am child of parent1 and parent2 "<<endl;
}
};
class multilevelinher : public singleinher
{
	public:
		multilevelinher()
		{
	cout<<"i am grandchild of parent1"<<endl;
}
};
class hierarchicalinher1 : public parent1
{
	public:
		hierarchicalinher1()
		{
			cout<<"i am 1st child of parent1"<<endl;
		}
};
class hierarchicalinher2 : public parent1
{
	public:
		hierarchicalinher2()
		{
			cout<<"i am 2nd child of parent1"<<endl;
		}
};
class hybridinher1 : public parent1
{
	
};
class hybridinher2 : public parent1,public parent2
{
	
};
int main()
{

cout<<"single inheritance : "<<endl;
singleinher obj;
cout<<"multiple inheritance : "<<endl;
multipleinher obj1;
cout<<"multilevel inheritance: "<<endl;
multilevelinher obj2;
cout<<"hierarchical inheritance :"<<endl;
hierarchicalinher1 obj3;
hierarchicalinher2 obj4;
cout<<"hybrid inheritance :"<<endl;
hybridinher2 obj5;
return 0;
}