 class parent1
{
    public
    parent1()
    {
System.out.println("i am parent1");
    }
}
class singleinher extends parent1{
public singleinher()
{
    System.out.println("i am single inherited child of parent1");
}
}
class multilevelinher extends parent1{
public multilevelinher()
{
    System.out.println("I am child of parent1");
}
}
class multilevelinher2 extends multilevelinher{
public multilevelinher2()
{
    System.out.println("i am grandson of parent1");
}
}
class hierarchical extends parent1{
public hierarchical()
{
    System.out.println("i am 1st child of parent1");
}
}
class hierarchical2 extends parent1{
public hierarchical2()
{
    System.out.println("i am 2nd child of parent2");
}
}
public class InheriTypesJava {
    public static void main(String[] args)
    {
        System.out.println("single inheritance : ");
        singleinher obj = new singleinher();
        System.out.println("Multi level inheritance:");
        multilevelinher2 obj1 = new multilevelinher2();
        System.out.println("Hierarchical inheritance:");
        hierarchical obj2 = new hierarchical();
        hierarchical2 obj3 = new hierarchical2();
    }
}
 