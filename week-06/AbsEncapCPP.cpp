#include<iostream>
using namespace std;
class AccessSpecifierDemo{
    private:
    int PriVar;
    protected:
    int ProVar;
    public:
    int PubVar;
    
    void setVar(int priVal,int pubVal,int proVal){
        PriVar=priVal;
        ProVar=proVal;
        PubVar=pubVal;


    }
    void getVar(){
        cout<<"The Private Variable Value is:"<<PriVar<<endl;
        cout<<"The Public variable Value is:"<<PubVar<<endl;
        cout<<"The protected Variable Value is:"<<ProVar<<endl;
    }  

};
int main(){
    AccessSpecifierDemo obj;
    obj.setVar(5,10,15);
    obj.getVar();
    return 0;
}